(defn guess[nr]
  #(cond 
    (> % nr) -1
    (< % nr) 1
    :else 0))

(defn next-guess[from to]
  (quot (+ from to) 2))

(defn guess-number[from to warm-cold]
  (loop [attempt 1 a (dec from) b (inc to)] 
      (let [nr (next-guess a b)]
        (if (< a nr b)
          (case (warm-cold nr)
              -1 (recur (inc attempt) a nr)
               1 (recur (inc attempt) nr b) 
               0 [nr attempt])
          :cheater))))

