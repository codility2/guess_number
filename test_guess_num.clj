(require '[clojure.spec.alpha :as spec])
(use 'clojure.test)
(load-file "guess_num.clj")

(def resp-ok (spec/tuple int? int?))
(def resp-cheat :cheater)

(deftest test-bound-a-guessed
    (is (spec/valid? resp-ok (guess-number 0 1 (guess 0)))))

(deftest bound-b-guessed
    (is (spec/valid? resp-ok (guess-number 0 1 (guess 1)))))

(deftest out-bound-a-cheat
    (is (= resp-cheat (guess-number 0 1 (guess -1)))))

(deftest out-bound-b-cheat
    (is (= resp-cheat (guess-number 0 1 (guess 2)))))

(deftest all-guessed
  (let [from 0 to 100]
    (doseq [nr (range from to)]
      (is (spec/valid? resp-ok (guess-number from to (guess nr)))))))

(clojure.test/run-tests)
